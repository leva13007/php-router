<?php


namespace app\core;


class HttpError
{
    static function methodNotAllowed($uri){
        http_response_code(405);
        header("{$uri} 405 Method Not Allowed");
        echo "{$uri} 405 Method Not Allowed";
    }

    static function notFound($uri){
        http_response_code(404);
        header("{$uri} 404 Not found");
        echo "{$uri} 404 Not found";
    }
}
