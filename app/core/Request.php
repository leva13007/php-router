<?php


namespace app\core;


class Request {
    public $requestUri;
    private $queryString;
    public $requestMethod;

    public function __construct()
    {
        $this->bootstrap();
    }

    function bootstrap()
    {
        $this->requestUri = rtrim(explode('?', $_SERVER['REQUEST_URI'])[0], ' /') ?: '/';
        $this->queryString = $_SERVER['QUERY_STRING'] ?? null;
        $this->requestMethod = $_SERVER['REQUEST_METHOD'];
    }


    // get properties $input->all(): array

    function getBody()
    {
//        echo 'get body';
    }
}
