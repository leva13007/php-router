<?php

namespace app\core;

use app\core\Request;

class Router {
    private $request;
    private $supportedMethods = [
        'GET',
        'POST',
    ];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    function __call(string $method, array $arguments)
    {
        list($route, $action) = $arguments;

        // todo check $action type(cb, string, array)
        $route = rtrim($route, '/') ?: '/';
        $this->{$method}[$route]['action'] = $action;
        $reg = preg_replace_callback_array([
                '#\/#' => function($match) {
                    return '\/';
                },
                '#\{(\w+)\}#' => function($match) {
                    return '(\w+)';
                },
            ],
            $route
        );
        $this->{$method}[$route]['reg'] = "#^{$reg}$#";
    }

    /**
     * @return array|bool
     */
    function match()
    {
        foreach ($this->{strtolower($this->request->requestMethod)} as $value){

            preg_match($value['reg'], $this->request->requestUri, $m);

            if (is_array($m) && isset($m[0])) {
                return [
                    'action' => $value['action'],
                    'arguments' => array_slice($m, 1),
                ];
            }
        }
        return false;
    }

    function resolve()
    {
        if(!in_array($this->request->requestMethod, $this->supportedMethods)){
            HttpError::methodNotAllowed($this->request->requestUri);
            return;
        }

        if(!$result = $this->match()) {
            HttpError::notFound($this->request->requestUri);
            return;
        }

        call_user_func_array($result['action'], $result['arguments']);
    }

    function __destruct()
    {
        $this->resolve();
    }
}
