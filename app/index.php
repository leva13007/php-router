<?php

namespace app;

ini_set('display_errors', 1);
error_reporting(E_ALL);

spl_autoload_register(function ($class) {
    $path = str_replace('\\', DIRECTORY_SEPARATOR, $class . '.php');
    include $path;
});

/* Create service provider here */
$request = new core\Request();
$router = new core\Router($request);

include 'app/route/web.php';
include 'app/route/api.php';

