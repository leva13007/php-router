<?php

return [
    $router->get('/', function (){
        echo "Root route";
    }),
    $router->get('/aaaa', function (){
        echo "aaaa route";
    }),
    $router->get('/bbbb', function (){
        echo "bbbb route";
    }),
    $router->get('/bbbb/{id}', function ($id){
        var_dump($id);
        echo "bbbb route, id: {$id}";
    }),
    $router->get('/cccc', function (){
        echo "cccc route";
    }),
    $router->get('/cccc/{id}/', function (){
        echo "cccc route";
    })
];
