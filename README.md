# php router

## Tasks:
Main - implement functionality

```php
Router::get('/path/{id}', function ($id) {
    // do smth 
    echo $id;
});
```

Todo:
1. ~~Request~~, Router(web and api) classes
2. ~~Parse args from URI~~
3. Response class (web and api)
4. ~~HttpError class~~
5. Service provider
6. Service container
7. Get response body

## Run project

```bash
php -S localhost:9000 ./app/index.php
```

paste our routes to './app/route/web.php'

---
Requirements
* File name and Class name must be the same
